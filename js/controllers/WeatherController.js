app.controller("WeatherController", function ($scope, $http) {
    $http({
        method: "GET",
        url: "https://s3.amazonaws.com/codecademy-content/courses/ltp4/forecast-api/forecast.json"
    }).then(function mySuccess(response) {
        $scope.fiveDay = response.data;
    }, function myError(response) {
        // $scope.fiveDay = response.statusText;
    });
});