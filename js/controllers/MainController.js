//A controller manages an app's data

/**
 * What is $scope?
 * 
 * $scope is defined in our html document, applying the directive ng-controller to an html element
 * will define the $scope of the applicable controller.
 * 
 * In this case here, our index.html contains <div class="main" ng-controller="MainController">...</div>
 * Therefore our $scope for MainController is everything incompassed by that div element.
 * 
 * This means that properties attached to $scope (i.e title) will become available to use within
 * <div class="main" ..>...
 */
app.controller("MainController", ['$scope', function ($scope) {
    //We're using title to store a string that will be used in the html file
    $scope.title = 'Top Sellers in Books';
    $scope.subTitle = 'New books arriving weekly';
    $scope.products =
        [
            {
                name: 'The Book of Trees',
                price: 19,
                pubdate: new Date('2014', '03', '08'),
                cover: 'img/the-book-of-trees.jpg',
                likes: 0,
            },
            {
                name: 'Program or be Programmed',
                price: 8,
                pubdate: new Date('2013', '08', '01'),
                cover: 'img/program-or-be-programmed.jpg',
                likes: 0,
            },
            {
                name: 'Hamish X and the Cheese Pirates',
                price: 8.99,
                pubdate: new Date('2007', '01', '05'),
                cover: 'img/Hamish-X-and-the-Cheese-Pirates.jpg',
                likes: 0,
            },
            {
                name: 'Sherlock Holmes: The Sign Of Four',
                price: 5.99,
                pubdate: new Date('1890', '02', '01'),
                cover: 'img/the-sign-of-four.jpg',
                likes: 0,
            }
        ];
    $scope.plusOne = function (index) {
        $scope.products[index].likes += 1;
    };
}]);