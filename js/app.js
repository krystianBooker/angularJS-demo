//A module contains the different components of an AngularJS app.
var app = angular.module("myApp", ['ngRoute', 'ngAnimate', 'ui.bootstrap']);
app.config(function($routeProvider){
    $routeProvider
      .when('/', {
        controller: 'MainController',
        templateUrl: 'views/home.html'
      })
      .when('/weather', {
        controller: 'WeatherController',
        templateUrl: 'views/weather.html'
      })
      .otherwise({
        redirectTo: '/'
      });
  });