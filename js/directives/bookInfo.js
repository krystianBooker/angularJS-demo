//https://docs.angularjs.org/guide/directive
app.directive('bookInfo', function () {
    return {
        //Specifies how the directive will be used in the view. 'E' means it will be used as a new HTML element.
        restrict: 'E', 
        scope: {
            //Scope specifies that we will pass information into this directive through an attirbute named info
            //'=' tells the directive to look for an atrribute named info in the <app-info> element
            info: '=',
            //'&' means this attribute will contain an expression and the directive code will want to execute that expression sometimes.
            // In this case it allows us to pass parameters
            updateLikes: '&'
        },
        //temlpateUrl specifies the HTML to use in order to display the data in scope.info
        templateUrl: 'js/directives/bookInfo.html'
    }
});
