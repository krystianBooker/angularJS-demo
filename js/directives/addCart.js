//https://docs.angularjs.org/guide/directive
app.directive('addCart', function () {
    return {
        //Specifies how the directive will be used in the view. 'E' means it will be used as a new HTML element.
        restrict: 'E',
        scope: {
            //Scope specifies that we will pass information into this directive through an attirbute named info
            //'=' tells the directive to look for an atrribute named info in the <app-info> element
            info: '=',
        },
        //temlpateUrl specifies the HTML to use in order to display the data in scope.info
        templateUrl: 'js/directives/addCart.html',
        link: function (scope, element, attrs) {
            scope.buttonText = "Add to Cart",
                scope.isInCart = false,
                scope.download = function () {
                    if(scope.isInCart){
                        scope.buttonText = "Add to Cart";
                        scope.isInCart = false;
                    } else {
                        scope.buttonText = "Remove from Cart";
                        scope.isInCart = true;
                    }
                }
        }
    }
});
